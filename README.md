# Procedure to build the conductance-based phenomenological model

## About the project

Our non-spiking model combines the biological plausibility of non-spiking conductance-based models (CBMs) with the high computational efficiency of phenomenological models. 

## Getting started

This repository contains the code to build the conductance-based phenomenological non-spiking model.

### Usage

The detailed procedure is given in the section “Design procedure of the model” of the paper cited below.

### Prerequisites
To run the code, it is necessary to have previously installed:
- [Python](https://www.python.org/)

### Structure of the project

 * ["Conductance-based model" folder](./conductance-based model)
   * [SSC_CBM.py](./conductance-based model/SSC_CBM.py): steady-state current (SSC) of the CBM
   * [SSC_CBM_variation_gCa.py](./conductance-based model/SSC_CBM_variation_gCa.py): SSC of the CBM with gCa as a parameter to vary
   * [current_clamp_CBM.py](./conductance-based model/current_clamp_CBM.py): current clamp of the CBM with the possibility to vary gCa

 * ["Procedure to design the model" folder](./Procedure to design the model)
 
   * [Step 1a](./Procedure to design the model/Step 1a)
     * [SSC_CBM_WT.py](./Procedure to design the model/Step 1a/SSC_CBM_WT.py): steady-state current of the wild-type conductance-based model
     * [4_points_SSC_CBM_WT_interpolation.py](./Procedure to design the model/Step 1a/4_points_SSC_CBM_WT_interpolation.py): 4 interpolation points of the WT SSC of the CBM (the two zeros, and the local minima and maxima of the SSC)

   * [Step 1b](./Procedure to design the model/Step 1b)
     * [SSC_variation_neuron_gCa.py](./Procedure to design the model/Step 1b/SSC_variation_neuron_gCa.py): SSC of the CBM with gCa as a parameter to vary
     * [arrayPointsForLowerThresholdPhenotype3&2.py](./Procedure to design the model/Step 1b/arrayPointsForLowerThresholdPhenotype3&2.py) and [arrayPointsForUpperThresholdPhenotype3&2.py](./Procedure to design the model/Step 1b/arrayPointsForUpperThresholdPhenotype3&2.py): determination of the local minima and maxima of the SSC for various gCa in phenotype 3 and 2
     * [automated_building_SSC_cubic_phenotype3&2.py](./Procedure to design the model/Step 1b/automated_building_SSC_cubic_phenotype3&2.py): computing of the coefficients a, b, c and d of the cubic function f for each new SSC generated from a new value of gCa

   * [Step 2](./Procedure to design the model/Step 2)
     * [poly_regression_coeff_a_phenotype3and2.py](./Procedure to design the model/Step 2/poly_regression_coeff_a_phenotype3and2.py): two-degree polynomial regression on coefficient a to determine the associated function u_i
     * [poly_regression_coeff_b_phenotype3and2.py](./Procedure to design the model/Step 2/poly_regression_coeff_b_phenotype3and2.py): two-degree polynomial regression on coefficient b to determine the associated function u_i
     * [poly_regression_coeff_c_phenotype3and2.py](./Procedure to design the model/Step 2/poly_regression_coeff_c_phenotype3and2.py): two-degree polynomial regression on coefficient c to determine the associated function u_i
     * [poly_regression_coeff_d_phenotype3and2.py](./Procedure to design the model/Step 2/poly_regression_coeff_d_phenotype3and2.py): two-degree polynomial regression on coefficient d to determine the associated function u_i
     * [h_gCa.py](./Procedure to design the model/Step 2/h_gCa.py): plot of the function h_gCa(V) for various values of gCa
     * [plot_voltage_simpleModel.py](./Procedure to design the model/Step 2/plot_voltage_simpleModel.py): plot of the conductance-based phenomenological non-spiking model

## Citing this model
If you use this model in your own research, we kindly ask you to cite our article:

> Naudin L., Raison-Aubry L., Buhry L. “Conductance-based phenomenological non-spiking model: a dimensionless and simple model that reliably predicts the effects of conductance variations on non-spiking neuronal dynamics.”

## Contact
Loïs NAUDIN - lois.naudin@gmail.com

