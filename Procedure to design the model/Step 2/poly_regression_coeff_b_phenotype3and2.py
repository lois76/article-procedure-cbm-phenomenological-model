from math import fabs
import numpy as np
from sklearn.linear_model import LinearRegression
from sklearn.preprocessing  import PolynomialFeatures
import matplotlib.pyplot as plt


gCa = np.arange(4.92, 3.62, -0.1).reshape((-1,1))
b = np.array([1.12941474, 1.13512722, 1.14044083, 1.14534321, 1.14956708, 1.1533837, 1.15642393, 1.15879288, 1.16010723, 1.16039121, 1.15966244, 1.15743079, 1.15390353])

transformer = PolynomialFeatures(degree=2, include_bias = False)
transformer.fit(gCa)
gCa_ = transformer.transform(gCa)
model = LinearRegression().fit(gCa_, b)
r_sq = model.score(gCa_, b)

b0 = model.intercept_
b1 = model.coef_[0]
b2 = model.coef_[1]


print("b0:", b0)
print("b1:", b1)
print("b2:", b2)

gCa_vec = np.arange(0.02, 4.93, 0.01)
f = b0 + b1*gCa_vec + b2*gCa_vec**2
plt.plot(gCa_vec, f, 'r', label = "polynomial regression")
plt.scatter(gCa,b, label = "interpolation points")
# plt.legend()
plt.xlabel(r"$g_{Ca}$")
plt.ylabel(r"$b$")
plt.show()

