import matplotlib.pyplot as plt
import numpy as np
#import SSC_variation_neuron	

# Poly 2

def a(gCa):
    a0 = 0.007639852543856089
    a1 = 0.004684656053946076
    a2 = -0.0004926338661338681
    return a0 + a1*gCa + a2*gCa**2
    
def b(gCa):
    b0 = 0.4865810416439508
    b1 = 0.33251734604395833
    b2 = -0.04107388461538485
    return b0 + b1*gCa + b2*gCa**2

def c(gCa):
    c0 = 17.642984643492806
    c1 = 5.3655602067533055
    c2 = -0.9874520869130944
    return c0 + c1*gCa + c2*gCa**2


def d(gCa):
    d0 = 361.2221419547018
    d1 = -26.783500080059657
    d2 = -5.403401757242819
    return d0 + d1*gCa + d2*gCa**2


def cubic_h(V, gCa):
    return a(gCa)*V**3 + b(gCa)*V**2 + c(gCa)*V + d(gCa)

f = np.vectorize(cubic_h)

V = np.arange(-50, 5, 0.01)
gCa_var = np.arange(4.92, 0, -0.2)

for gCa in gCa_var:
    plt.plot(V, f(V, gCa), label = gCa)

plt.xlabel(r"$V\;\; (mV)$")
plt.ylabel(r"$f(V)\;\; (pA)$")
plt.title("simple model")
plt.legend()
plt.ylim(-100,100)
plt.xlim(-50,5)
plt.show()
