from math import fabs
import numpy as np
from sklearn.linear_model import LinearRegression
from sklearn.preprocessing  import PolynomialFeatures
import matplotlib.pyplot as plt


gCa = np.arange(4.92, 3.62, -0.1).reshape((-1,1))
a = np.array([0.01877761, 0.01877559, 0.01876909, 0.01875832, 0.01873869, 0.01871537, 0.01868254, 0.01864219, 0.01858825, 0.01852205, 0.01844365, 0.01834639, 0.01823354])

transformer = PolynomialFeatures(degree=2, include_bias = False)
transformer.fit(gCa)
gCa_ = transformer.transform(gCa)
model = LinearRegression().fit(gCa_, a)
r_sq = model.score(gCa_, a)

a0 = model.intercept_
a1 = model.coef_[0]
a2 = model.coef_[1]


print("a0:", a0)
print("a1:", a1)
print("a2:", a2)

gCa_vec = np.arange(3.71, 4.93, 0.01)
f = a0 + a1*gCa_vec + a2*gCa_vec**2
plt.plot(gCa_vec, f, 'r', label = "polynomial regression")
plt.scatter(gCa, a, label = "interpolation points")
#plt.legend()
plt.xlabel(r"$g_{Ca}$")
plt.ylabel(r"$a$")
plt.show()


