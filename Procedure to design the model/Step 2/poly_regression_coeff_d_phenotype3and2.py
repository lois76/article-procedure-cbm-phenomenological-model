from math import fabs
import numpy as np
from sklearn.linear_model import LinearRegression
from sklearn.preprocessing  import PolynomialFeatures
import matplotlib.pyplot as plt


gCa = np.arange(4.92, 3.62, -0.1).reshape((-1,1))
d = np.array([98.85116096, 106.602583,   114.32291859, 122.00213917, 129.62595879, 137.18591735, 144.66100608, 152.03877217, 159.29133716, 166.39684826, 173.3395237, 180.07049801, 186.57784207])

transformer = PolynomialFeatures(degree=2, include_bias = False)
transformer.fit(gCa)
gCa_ = transformer.transform(gCa)
model = LinearRegression().fit(gCa_, d)
r_sq = model.score(gCa_, d)

d0 = model.intercept_
d1 = model.coef_[0]
d2 = model.coef_[1]


print("d0:", d0)
print("d1:", d1)
print("d2:", d2)

gCa_vec = np.arange(3.71, 4.93, 0.01)
f = d0 + d1*gCa_vec + d2*gCa_vec**2
plt.plot(gCa_vec, f, 'r', label = "polynomial regression")
plt.scatter(gCa, d, label = r"$(g_{Ca},x),\;x\in\{a,b,c,d\}$")
#plt.legend()
plt.xlabel(r"$g_{Ca}$")
plt.ylabel(r"$d$")
plt.show()


