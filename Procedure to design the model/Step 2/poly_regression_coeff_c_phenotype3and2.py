from math import fabs
import numpy as np
from sklearn.linear_model import LinearRegression
from sklearn.preprocessing  import PolynomialFeatures
import matplotlib.pyplot as plt


gCa = np.arange(4.92, 3.62, -0.1).reshape((-1,1))
c = np.array([20.1651401, 20.56544333, 20.9561989, 21.33632502, 21.70212509, 22.05597638, 22.39188842, 22.71057832, 23.00575667, 23.27572268, 23.52070124, 23.73058147, 23.90812541])

transformer = PolynomialFeatures(degree=2, include_bias = False)
transformer.fit(gCa)
gCa_ = transformer.transform(gCa)
model = LinearRegression().fit(gCa_, c)
r_sq = model.score(gCa_, c)

c0 = model.intercept_
c1 = model.coef_[0]
c2 = model.coef_[1]


print("c0:", c0)
print("c1:", c1)
print("c2:", c2)

gCa_vec = np.arange(0.02, 4.93, 0.01)
f = c0 + c1*gCa_vec + c2*gCa_vec**2
plt.plot(gCa_vec, f, 'r', label = "polynomial regression")
plt.scatter(gCa, c, label = "interpolation points")
# plt.legend()
plt.xlabel(r"$g_{Ca}$")
plt.ylabel(r"$c$")
plt.show()


