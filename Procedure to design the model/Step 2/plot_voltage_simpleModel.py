import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
from scipy.integrate import odeint
from math import exp

def a(gCa):
    a0 = 0.007639852543856089
    a1 = 0.004684656053946076
    a2 = -0.0004926338661338681
    return a0 + a1*gCa + a2*gCa**2
    
def b(gCa):
    b0 = 0.4865810416439508
    b1 = 0.33251734604395833
    b2 = -0.04107388461538485
    return b0 + b1*gCa + b2*gCa**2

def c(gCa):
    c0 = 17.642984643492806
    c1 = 5.3655602067533055
    c2 = -0.9874520869130944
    return c0 + c1*gCa + c2*gCa**2


def d(gCa):
    d0 = 361.2221419547018
    d1 = -26.783500080059657
    d2 = -5.403401757242819
    return d0 + d1*gCa + d2*gCa**2


def f(V, gCa):
    return a(gCa)*V**3 + b(gCa)*V**2 + c(gCa)*V + d(gCa)


tau = 16

    
def Istim(I,t):
    if t<=500 or t>=2500:
        return 0
    else:
        return I

g = np.vectorize(Istim)


# Simple model
def simpleModel(y, t, I):
    V = y

    dV = (1/tau)*(-f(V, 2.02) + g(I,t))

    return dV

# Initial conditions
V0 = -34


# Numerical integration with scipy.odeint data
time = np.arange(0, 3000, 0.01)
Istim = np.arange(-15, 40, 5)
for I in Istim:
    orbit = odeint(simpleModel, V0, time, args=(I,))
    plt.plot(time, orbit[:, 0], label=I)

plt.ylim(-40,0)
plt.xlabel("t (ms)")
plt.ylabel("V(t) (mV)")
#plt.title("simple model")
# plt.legend()
plt.show()

