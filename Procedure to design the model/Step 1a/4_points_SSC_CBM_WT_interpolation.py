from SSC_CBM_WT import SS_Cone
import numpy as np
from scipy.optimize import fsolve

vecV2 = np.arange(-40, -21, 0.0001)
vecV3 = np.arange(-20, 0, 0.0001)

SS2 = []
SS3 = []

# Determination of the first zero of the SS function
V1 = fsolve(SS_Cone, -50)
Iinf1 = SS_Cone(V1)
print(V1)
print(Iinf1)
print("========")

# Determination of the local maxima of the left knee
for i in range(len(vecV2)):
    SS2.append(SS_Cone(vecV2[i]))

ind = SS2.index(max(SS2)) # V index of the maximum
V2 = vecV2[ind]
Iinf2 = SS2[ind]
print(V2)
print(Iinf2)
print("========")

# Determination of the local minima of the right knee
for i in range(len(vecV3)):
    SS3.append(SS_Cone(vecV3[i]))

ind = SS3.index(min(SS3)) # V index of the minimum
V3 = vecV3[ind]
Iinf3 = SS3[ind]
print(V3)
print(Iinf3)
print("========")

# Determination of the second zero of the SS function
V4 = fsolve(SS_Cone, 20)
Iinf4 = SS_Cone(V4)
print(V4)
print(Iinf4)
