import matplotlib.pyplot as plt
from scipy.optimize import fsolve
from SSC_variation_neuron_gCa import SS_Cone
import numpy as np

vecV_upper = np.arange(-40, -21, 0.01)

V2 = [] # ...
Iinf2 = [] # ...

gCa_var = np.arange(4.92, 3.52, -0.1)

for i in range(np.size(gCa_var)):
	Iinf_upper = []
	for j in range(len(vecV_upper)):
    		Iinf_upper.append(SS_Cone(vecV_upper[j], gCa_var[i]))
    		
	index = Iinf_upper.index(max(Iinf_upper)) # V index of the maximum
	V2.append(vecV_upper[index])
	Iinf2.append(Iinf_upper[index])
	
print(V2)
print(Iinf2)



