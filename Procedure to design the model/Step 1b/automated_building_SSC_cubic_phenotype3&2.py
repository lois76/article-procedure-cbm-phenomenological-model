import numpy as np
from scipy.interpolate import lagrange
from numpy.polynomial.polynomial import Polynomial
import matplotlib.pyplot as plt

V_upper = [-25.750000000002835, -25.510000000002883, -25.260000000002933, -24.990000000002986, -24.72000000000304, -24.4200000000031, -24.100000000003163, -23.750000000003233, -23.380000000003307, -22.96000000000339, -22.490000000003484, -21.930000000003595, -21.220000000003736, -21.010000000003778]
#print(np.size(V_upper))


V_lower = [-13.459999999998978, -13.639999999999006, -13.829999999999036, -14.039999999999068, -14.259999999999103, -14.489999999999139, -14.74999999999918, -15.029999999999223, -15.339999999999272, -15.699999999999328, -16.09999999999939, -16.59999999999947, -17.23999999999957, -18.319999999999737]
#print(np.size(V_lower))


Iinf_upper = [7.865106107226076, 8.982200402498115, 10.135302431963268, 11.32717431040657, 12.561000169501988, 13.840453994214982, 15.16985247213438, 16.554372473705776, 18.000379400444608, 19.51589573758072, 21.111570383303416, 22.802529843977688, 24.613133649363085, 26.533766948107697]
#print(np.size(Iinf_upper))

Iinf_lower = [-13.74425842672575, -10.367211817089355, -7.01935095790158, -3.7032618726939432, -0.4219161564653007, 2.821202709476921, 6.02199155558678, 9.17549165560304, 12.275587288043852, 15.3144794582022, 18.281783006264604, 21.162637304027285, 23.932965252171925, 26.539551515754354]
#print(np.size(Iinf_lower))

def V(m):
	V1 = -39.87589944 - 0.05090283*m #-100pA
	V2 = V_upper[m] # V(upper threshold)
	V3 = V_lower[m] # V(lower tcdhreshold)
	V4 = 0.05679073 - 0.38371027*m #100pA
	return [V1, V2, V3, V4] 

def Iinf(m):
	Iinf1 = -100
	Iinf2 = Iinf_upper[m]
	Iinf3 = Iinf_lower[m]
	Iinf4 = 100 
	return [Iinf1, Iinf2, Iinf3, Iinf4]

def cubique(V):
	return d + c*V + b*V**2+ a*V**3

A = np.zeros(0)
B = np.zeros(0)
C = np.zeros(0)
D = np.zeros(0)


for m in range(13):
	poly = lagrange(V(m), Iinf(m))
	a = Polynomial(poly.coef[::-1]).coef[3]
	b = Polynomial(poly.coef[::-1]).coef[2]
	c = Polynomial(poly.coef[::-1]).coef[1]
	d = Polynomial(poly.coef[::-1]).coef[0]
	
	A = np.append(A, a)
	B = np.append(B, b)
	C = np.append(C, c)
	D = np.append(D, d)

	# Plot cubic
	
	f = np.vectorize(cubique)
	vecV = np.arange(-50, 5, 0.01)
	plt.plot(vecV, f(vecV), label = round(4.92-0.1*m,2))
	plt.xlim(-50,10)
	plt.ylim(-100,100)
	plt.legend()
	
	#plt.show()



print("A = ", A)
print("B = ", B)
print("C = ", C)
print("D = ", D)


#plt.show()



