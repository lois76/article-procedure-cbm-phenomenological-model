import matplotlib.pyplot as plt
from scipy.optimize import fsolve
from SSC_variation_neuron_gCa import SS_Cone
import numpy as np

vecV_lower = np.arange(-20, 0, 0.01)

V3 = [] # ...
Iinf3 = [] # ...

gCa_var = np.arange(4.92,3.52,-0.1)

for i in range(np.size(gCa_var)):
	Iinf_lower = []
	for j in range(len(vecV_lower)):
    		Iinf_lower.append(SS_Cone(vecV_lower[j], gCa_var[i]))
    		
	index = Iinf_lower.index(min(Iinf_lower)) # V index of the maximum
	V3.append(vecV_lower[index])
	Iinf3.append(Iinf_lower[index])
	
print(V3)
print(Iinf3)



