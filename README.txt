"conductance-based model (CBM)" folder:
	- SSC_CBM.py: steady-state current (SSC) of the CBM
	- SSC_CBM_variation_gCa.py: SSC of the CBM with gCa as a parameter to vary
	- current_clamp_CBM.py: current clamp of the CBM with the possibility to vary gCa

"Procedure to design the model" folder:

	"Step 1a" sub-folder:
		- SSC_CBM_WT.py: steady-state current of the wild-type conductance-based model
		- 4_points_SSC_CBM_WT_interpolation.py: 4 interpolation points of the WT SSC of the CBM (the two zeros, and the local minima and maxima of the SSC). 
		
	"Step 1b" sub-folder:
		- arrayPointsForLowerThresholdPhenotype3&2.py and arrayPointsForUpperThresholdPhenotype3&2.py: determination of the local minima and maxima of the SSC for various gCa in phenotype 3 and 2.
		- arrayPointsFor100pAPhenotype1&2.py and arrayPointsFor-100pAPhenotype1&2.py: determination of the V-coordinate in the upper and lower bound, respectively, of the SSC for various gCa in phenotype 3 and 2.
		- automated_building_SSC_cubic_phenotype3&2.py: computing of the coefficients a, b, c and d of the cubic function f for each new SSC generated from a new value of gCa
	
	"Step 2 - polynomial regression" sub-folder:
		- poly_regression_coeff_x_phenotype3and2.py, x in {a,b,c,d}: two-degree polynomial regression on each coefficient x to determine the associated function u_i
		- new_simple_model_gCa_phenotype3and2.py: plot of the function h_gCa(V) for various values of gCa
		- plot_voltage_simpleModel.py: plot of the conductance-based phenomenological non-spiking model. 
		
