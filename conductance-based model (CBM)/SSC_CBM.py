import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import random
from scipy.integrate import odeint
import math
from math import exp

# Global parameters of the cone cell
Cm_Cone = 16 #nF
v0_Cone = -29 #mV

# Ionic parameters
#=== Ca ===
aoCa_Cone = 3.1
eCa_Cone = 40 #mV
SCa_Cone = 5.7 #mV
VhalfCa_Cone = -16.6 #mV
gCa_Cone = 3.59 # 4.92 #nS
#=== h ===
eh_Cone = -32.5 #mV
gh_Cone = 3.5 #nS
#=== Kv ===
eKv_Cone = -80 #mV
gKv_Cone = 2 #nS
#=== Leak ===
eleak_Cone = -33.5 #mV
gleak_Cone = 5.8 #nS

# Leak current
def Ileak(v_Cone):
    return gleak_Cone*(v_Cone-eleak_Cone)

# Calcium current
def alphanCa_Cone(v_Cone):
    return aoCa_Cone*math.exp((v_Cone-VhalfCa_Cone)/(2*SCa_Cone))

def betanCa_Cone(v_Cone):
    return aoCa_Cone*math.exp((-v_Cone+VhalfCa_Cone)/(2*SCa_Cone))

def eq_nCa_Cone(v_Cone):
    return alphanCa_Cone(v_Cone)/(alphanCa_Cone(v_Cone)+betanCa_Cone(v_Cone))

def eq_ICa(v_Cone):
    return gCa_Cone*eq_nCa_Cone(v_Cone)*(v_Cone-eCa_Cone)

# Hyperpolarization activated current
def alphanh_Cone(v_Cone):
    return 18/(1+math.exp((v_Cone+88)/12))

def betanh_Cone(v_Cone):
    return 18/(1+math.exp((-v_Cone-18)/19))

def eq_nh_Cone(v_Cone):
    return alphanh_Cone(v_Cone)/(alphanh_Cone(v_Cone)+betanh_Cone(v_Cone))

def eq_Ih(v_Cone):
    return gh_Cone*(1-(1+3*eq_nh_Cone(v_Cone))*((1-eq_nh_Cone(v_Cone))**3))*(v_Cone-eh_Cone)

# Delayed rectifying potassium current
def alphamKv_Cone(v_Cone):
    return 5*(v_Cone-100)/(1-math.exp((100-v_Cone)/42))

def betamKv_Cone(v_Cone):
    return 9*math.exp((20-v_Cone)/40)

def alphahKv_Cone(v_Cone):
    return 0.15*math.exp(-v_Cone/22)

def betahKv_Cone(v_Cone):
    return 0.4125/(1+math.exp((10-v_Cone)/7))

def eq_mKv_Cone(v_Cone):
    return alphamKv_Cone(v_Cone)/(alphamKv_Cone(v_Cone)+betamKv_Cone(v_Cone))

def eq_hKv_Cone(v_Cone):
    return alphahKv_Cone(v_Cone)/(alphahKv_Cone(v_Cone)+betahKv_Cone(v_Cone))

def eq_IKv(v_Cone):
    return gKv_Cone*eq_mKv_Cone(v_Cone)*eq_mKv_Cone(v_Cone)*eq_mKv_Cone(v_Cone)*eq_hKv_Cone(v_Cone)*(v_Cone-eKv_Cone)

# Steady-state current of the cone cell (Kourenniy et al., 2004, with some modifications (see text))
def SS_Cone(v_Cone):
    return eq_ICa(v_Cone)+eq_Ih(v_Cone)+eq_IKv(v_Cone)+Ileak(v_Cone)

# Numerical simulation of the steady-state current of the cone cell

f2 = np.vectorize(SS_Cone)
V = np.arange(-50, 5, 0.01)
plt.plot(V, f2(V))
#plt.xlim(-40,0)
#plt.ylim(-30,30)
plt.xlabel(r"$V\;\; (mV)$")
plt.ylabel(r"$I_{\infty}(V)\;\; (pA)$")
plt.axhline(y=0)
plt.show()

