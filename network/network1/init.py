from brian2 import *
from eqs_sensoryNeurons import *
from eqs_bistableCBM import *
from eqs_bistableSM import * 

# sensory neurons (sn) 
sn = NeuronGroup(4, eqs_sn, method = "rk4")
sn.V = -33.4 # * mV
sn.I_ext = 10
sn.s = 0

# bistable neuron CBM (bn)
bn_CBM = NeuronGroup(1, eqs_bistableCBM, method = "exponential_euler")
bn_CBM.V = -29.3 * mV
bn_CBM.mKv = 0.3709
bn_CBM.hKv = 0.9998
bn_CBM.nCa = 0
bn_CBM.nh = 0.0877

# bistable neuron SM (bn)
bn_SM = NeuronGroup(1, eqs_bistableSM, method="rk4")
bn_SM.V = -31 

# # sensory neurons (sn)
# EXT = arange(-15,40,5)
# for k in range(11):
#     start_scope()
#     sn = NeuronGroup(rows_sn*cols_sn, eqs_sn, method="rk4")
#     sn.V = -38 # * mV
#     sn.I_ext = IEXT[k]  
#     sn.x = '(i // rows_sn) * distance_sn_sn - rows_sn/2.0 * distance_sn_sn'
#     sn.y = '(i % rows_sn) * distance_sn_sn - cols_sn/2.0 * distance_sn_sn'
#     sn.s = 0
#     sn_mon = StateMonitor(sn, ["V"], record = True)
#     run(7000*ms, report='text')
#     plot(sn_mon.t/ms, sn_mon.V[0])
# show()