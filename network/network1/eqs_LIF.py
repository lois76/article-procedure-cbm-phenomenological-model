from brian2 import *
import matplotlib.pyplot as plt

from brian2 import *
gL = 0.1    * nS
EL = -65    * mV
C = 16     * pF
VT = -55    * mV
VR = EL     

eqs = """
dV/dt = (gL*(EL - V) + I)/C : volt
I : amp
"""

neuron = NeuronGroup(1, model = eqs, threshold = "V > VT", reset = "V = VR", refractory = 0.5 * ms, method = "euler")
neuron.V = EL
trace = StateMonitor(neuron, 'V', record = 0)
spikes = SpikeMonitor(neuron)
neuron.I = 2*pA
run(1000 * ms)


V = trace[0].V[:]
for t in spikes.t:
    i = int(t / defaultclock.dt)
    V[i] = 20*mV

plot(trace.t / ms, V / mV)
xlabel('time (ms)')
ylabel('membrane potential (mV)')
show()

