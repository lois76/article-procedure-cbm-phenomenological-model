from brian2 import *

# parameters of the bistable simple model 
def a(gCa):
    a0 = 0.007639852543856089
    a1 = 0.004684656053946076
    a2 = -0.0004926338661338681
    return a0 + a1*gCa + a2*gCa**2
    
def b(gCa):
    b0 = 0.4865810416439508
    b1 = 0.33251734604395833
    b2 = -0.04107388461538485
    return b0 + b1*gCa + b2*gCa**2

def c(gCa):
    c0 = 17.642984643492806
    c1 = 5.3655602067533055
    c2 = -0.9874520869130944
    return c0 + c1*gCa + c2*gCa**2

def d(gCa):
    d0 = 361.2221419547018
    d1 = -26.783500080059657
    d2 = -5.403401757242819
    return d0 + d1*gCa + d2*gCa**2

def f(V, gCa):
    return a(gCa)*V**3 + b(gCa)*V**2 + c(gCa)*V + d(gCa)

gCa_bn = 4.92
a_bn = a(gCa_bn)
b_bn = b(gCa_bn)
c_bn = c(gCa_bn)
d_bn = d(gCa_bn)
tau_bn = 16 * ms

# Synapse parameters
gSyn = 0.1      * nS
ESyn = 0      * mV
weight = 1

# Model of sensory neurons (sn)
eqs_bistableSM = '''
dV/dt = (- a_bn*V**3 - b_bn*V**2 - c_bn*V - d_bn - Isyn)/tau_bn : 1

s_in  : 1
Isyn = gSyn/nS*s_in*(V-ESyn/mV) : 1   # sntobn
'''



