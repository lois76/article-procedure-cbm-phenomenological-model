from brian2 import *

# Global parameters of the cone cell
Cm = 16            * pF

# Ionic parameters
#=== Ca ===
aoCa = 3.1         / second
ECa = 40           * mV
SCa = 5.7          * mV
VhalfCa = -16.6    * mV
gCa = 4.92         * nS
#=== h ===
Eh = -32.5         * mV
gh = 3.5           * nS
#=== Kv ===
EKv = -80          * mV
gKv = 2            * nS
#=== Leak ===
EL = -33.5         * mV
gL = 5.8           * nS

# Synapse parameters
gSyn = 1 * nS
ESyn = 0 * mV
weight = 1

eqs_bistableCBM = """
anCa = ((3.1*10**3/second)*exp((V-VhalfCa)/(2*SCa))) : Hz
bnCa = ((3.1*10**3/second)*exp((-V+VhalfCa)/(2*SCa))) : Hz

anh = (18 / (1 + exp((V/mV + 88 ) / 12)))/ms : Hz
bnh = (18 / (1 + exp((-V/mV - 18) / 19)))/ms : Hz

amKv = (5*(V/mV - 100)/(1 - exp((100 - V/mV) / 42)))/ms : Hz
bmKv = (9 * exp((20 - V/mV) / 40))/ms : Hz
ahKv = (0.15 * exp(-(V/mV)/22))/ms : Hz
bhKv = (0.4125/(1 + exp((10 - V/mV) / 7)))/ms : Hz

dmKv/dt = amKv*(1-mKv) - bmKv*mKv : 1
dhKv/dt = ahKv*(1-hKv) - bhKv*hKv : 1
dnh/dt  = anh*(1-nh)   - bnh*nh	  : 1
dnCa/dt = anCa*(1-nCa) - bnCa*nCa : 1

s_in : 1
Isyn = gSyn*s_in*(V-ESyn) : amp # sntobn

dV/dt = (- gCa*nCa*(V-ECa)
    - gh*(1-(1+3*nh)*((1-nh)**3))*(V-Eh)
    - gKv*mKv*mKv*mKv*hKv*(V-EKv)
	- gL*(V-EL)
    - Isyn
	)/Cm  : volt

"""



