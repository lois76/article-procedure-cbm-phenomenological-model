from brian2 import *

# parameters of a near-linear neuron 
pa_sn = [0.0000242, 0.0036173, 0.3100748, 7.2228548, 0.042]
a_sn = pa_sn[0]
b_sn = pa_sn[1]
c_sn = pa_sn[2]
d_sn = pa_sn[3]
tau_sn = pa_sn[4]*100*ms

# parameters of synapse
Vslope = 10    # * mV
Vth = -10      # * mV
tau_s = 10      * ms   

# Model of sensory neurons (sn)
eqs_sn = '''
dV/dt = (I_ext*int(t>500*ms and t<2500*ms) - a_sn*V**3 - b_sn*V**2 - c_sn*V - d_sn)/tau_sn : 1

ds/dt = (1/(1+exp((Vth - V) / Vslope)) - s)/tau_s : 1 

# ds/dt = (tanh((V - Vth) / Vslope) * int(V > Vth) - s) / ((1-tanh((V - Vth) / Vslope) * int(V > Vth)) * 10*ms) : 1 # sntobn

I_ext : 1
'''



