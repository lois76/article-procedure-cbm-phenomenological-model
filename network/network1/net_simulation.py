from brian2 import *
from syn import *
from init import *

# Monitor state variable
bn_mon_cbm = StateMonitor(bn_CBM, ["V", "Isyn"], record = True)
bn_mon_sm = StateMonitor(bn_SM, ["V", "Isyn"], record = True)
sn_mon = StateMonitor(sn, ["V","s"], record = True)

# simulation parameters
#defaultclock.dt = 0.03 * ms
duration = 3000*ms 				# simulation total duration

# run simulation
run(duration, report='text')

# Show connectivity
show_connectivity = False
show_plot_sm = True
show_plot_cbm = False

if show_connectivity:
    figure(1)
    plot(sn.x/umeter, sn.y/umeter, 'ok', label='sn')
    for color in ['r']: #['g', 'b', 'm', 'r', 'c']:
        neuron_idx = 5 #np.random.randint(0, rows_sn*cols_sn)
        plot(sn.x[neuron_idx] / umeter, sn.y[neuron_idx] / umeter, 'o', mec=color,
            mfc='none', ms=10)																		# label = 'chosen cones'
        plot(sn.x[Sgap_sn1_sn2.j[neuron_idx, :]] / umeter,
            sn.y[Sgap_sn1_sn2.j[neuron_idx, :]] / umeter, color + 'o') 				# label='rod contacts'
    title('tmtc')
    legend()
    show()

if show_plot_sm:
    figure(2)
    plot(bn_mon_sm.t/ms, bn_mon_sm.V[0])
    title("Bistable SM")
    figure(3)
    plot(bn_mon_sm.t/ms, -(bn_mon_sm.Isyn[0]))
    title("Synaptic current Isyn in SM")
    figure(6)
    plot(sn_mon.t/ms, sn_mon.V[0])
    title("Vpre")
    figure(7)
    plot(sn_mon.t/ms, sn_mon.s[0])
    title("variable s")
    show()

if show_plot_cbm:
    figure(4)
    plot(bn_mon_cbm.t/ms, bn_mon_cbm.V[0]/mV)
    title("Bistable CBM")
    figure(5)
    plot(bn_mon_cbm.t/ms, -(bn_mon_cbm.Isyn[0])/pA)
    title("Synaptic current Isyn in CBM")
    figure(6)
    plot(sn_mon.t/ms, sn_mon.V[0])
    figure(7)
    plot(sn_mon.t/ms, sn_mon.s[0])
    title("variable s")
    # figure(8)
    # plot(sn_mon.V[0], 1/(1+exp((Vth - sn_mon.V[0]) / Vslope)))
    show()
