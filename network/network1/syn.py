from brian2 import *
from init import *

# gap junctions between sensory neurons
# Sgap_sn1_sn2 = Synapses(sn,sn, '''
				# g : 1 # gap junction maximal conductance
         		# Igap_post = g * (V_post - V_pre) : 1 (summed)
				# ''')

# Sgap_sn1_sn2.connect('sqrt((x_pre - x_post)**2 + (y_pre - y_post)**2) < (distance_sn_sn+1*um) and i!=j')
# Sgap_sn1_sn2.g = 0.1 # * nS

# chemical synapse from sensory neurons to the bistable CBM
# S_sn_bisCBM = Synapses(sn, bn_CBM,
# 		        's_in_post = weight*s_pre : 1 (summed)')

# S_sn_bisCBM.connect()

# chemical synapse from sensory neurons to the bistable SM
S_sn_bisSM = Synapses(sn, bn_SM,
		        's_in_post = weight*s_pre : 1 (summed)')

S_sn_bisSM.connect()

