import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import random
from scipy.integrate import odeint
import math
from math import exp

# Global parameters of the cone cell
Cm_Cone = 16 #nF
v0_Cone = -34 #mV

# Ionic parameters
#=== Ca ===
aoCa_Cone = 3.1
eCa_Cone = 40 #mV
SCa_Cone = 5.7 #mV
VhalfCa_Cone = -16.6 #mV
#gCa_Cone = 3.92 #nS
#=== h ===
eh_Cone = -32.5 #mV
gh_Cone = 3.5 #nS
#=== Kv ===
eKv_Cone = -80 #mV
gKv_Cone = 2 #nS
#=== Leak ===
eleak_Cone = -33.5 #mV
gleak_Cone = 5.8 #nS

# Leak current
def Ileak(v_Cone):
    return gleak_Cone*(v_Cone-eleak_Cone)

# Calcium current (ICa)
def alphanCa_Cone(v_Cone):
    return aoCa_Cone*math.exp((v_Cone-VhalfCa_Cone)/(2*SCa_Cone))

def betanCa_Cone(v_Cone):
    return aoCa_Cone*math.exp((-v_Cone+VhalfCa_Cone)/(2*SCa_Cone))

def ICa(gCa_Cone, nCa_Cone, v_Cone):
    return gCa_Cone*nCa_Cone*(v_Cone-eCa_Cone)

# Hyperpolarization activated current (Ih)
def alphanh_Cone(v_Cone):
    return 18/(1+math.exp((v_Cone+88)/12))

def betanh_Cone(v_Cone):
    return 18/(1+math.exp((-v_Cone-18)/19))

def Ih(nh_Cone, v_Cone):
    return gh_Cone*(1-(1+3*nh_Cone)*((1-nh_Cone)**3))*(v_Cone-eh_Cone)

# Delayed rectifying potassium current (IKv)
def alphamKv_Cone(v_Cone):
    return 5*(v_Cone-100)/(1-math.exp((100-v_Cone)/42))

def betamKv_Cone(v_Cone):
    return 9*math.exp((20-v_Cone)/40)

def alphahKv_Cone(v_Cone):
    return 0.15*math.exp(-v_Cone/22)

def betahKv_Cone(v_Cone):
    return 0.4125/(1+math.exp((10-v_Cone)/7))

def IKv(mKv_Cone, hKv_Cone, v_Cone):
    return gKv_Cone*mKv_Cone*mKv_Cone*mKv_Cone*hKv_Cone*(v_Cone-eKv_Cone)

# Current injection
def Istim(I,t):
     if t<=500 or t>=2500:
         return 0
     else:
         return I

# Current injection
'''
def Istim(I,t):
    if t>=200 and t<=300:
        return 20
    elif t<=500 or t>=2500:
        return 0
    else:
        return I
'''
f = np.vectorize(Istim)

# Neuronal cell system
def HH_cone(y, t, I, gCa_Cone):
    v_Cone, nCa_Cone, nh_Cone, mKv_Cone, hKv_Cone = y

    #dv_Cone/dt
    dv_Cone = (1/Cm_Cone)*(-Ih(nh_Cone, v_Cone)-IKv(mKv_Cone, hKv_Cone, v_Cone)-ICa(gCa_Cone, nCa_Cone, v_Cone)-Ileak(v_Cone)+f(I,t))
    #dnCa_Cone/dt
    dnCa_Cone = alphanCa_Cone(v_Cone)*(1-nCa_Cone)-betanCa_Cone(v_Cone)*nCa_Cone
    #dnh_Cone/dt
    dnh_Cone = alphanh_Cone(v_Cone)*(1-nh_Cone)-betanh_Cone(v_Cone)*nh_Cone
    #dmKv_Cone/dt
    dmKv_Cone = alphamKv_Cone(v_Cone)*(1-mKv_Cone)-betamKv_Cone(v_Cone)*mKv_Cone
    #dhKv_Cone/dt
    dhKv_Cone = alphahKv_Cone(v_Cone)*(1-hKv_Cone)-betahKv_Cone(v_Cone)*hKv_Cone

    return([dv_Cone, dnCa_Cone, dnh_Cone, dmKv_Cone, dhKv_Cone])

# Initial conditions of the neuronal cell
nCa0_Cone = 0.9915
nh0_Cone = 0.9877
mKv0_Cone = 0.99709    #Calculé
hKv0_Cone = 0.9998
condini = [v0_Cone, nCa0_Cone, nh0_Cone, mKv0_Cone, hKv0_Cone]

# Numerical integration with scipy.odeint data

time = np.arange(0, 3000, 0.01)
Istim = np.arange(-15, 40, 5)
gCa_var = [3.12, 3.32, 3.52, 3.72, 3.92, 4.12]


for i in range(len(gCa_var)):
	plt.figure(i)
	for I in Istim:
		orbit = odeint(HH_cone, condini, time, args=(I, gCa_var[i]))
		plt.plot(time, orbit[:, 0], label=I)

plt.xlabel("t (ms)")
plt.ylabel("V(t) (mV)")
plt.ylim(-40,0)
#plt.title(r"$g_{Ca}=2.02$nS")
plt.legend()

plt.show()

